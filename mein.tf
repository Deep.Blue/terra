provider "aws"{
    region = "eu-central-1"
    profile = "default"
 
}

variable "subnet_cidr"{
    description = "subnet cidr"
    type = string
}
variable "subnet_cidr1"{
    description = "subnet cidr"
    type = list(object({
        cidr_block=string
        name=string
    }))
}



resource "aws_vpc" "sample_vpc"{
    cidr_block = "172.32.0.0/16"
    
    tags = {
        Name: "terraformVpc"
        env:"terraform"
    }
}

resource "aws_subnet" "sample_subnet"{
    cidr_block = var.subnet_cidr   
    vpc_id = aws_vpc.sample_vpc.id
    availability_zone = "eu-central-1a"
    tags = {
        Name: "sampleTerraform"
        env:"terraform"
    }
}

output "my_vpc_id"{
    value=aws_vpc.sample_vpc.id
}


output "subnet_arn"{
    value = aws_subnet.sample_subnet.arn
}